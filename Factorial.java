//Code to calculate factorial of a number 
// We need to import scanner class
import java.util.Scanner;

//The main class
// Start of Factorial class
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();

 //Test end case
 // Check if number is negative. If so then return error
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {

//Actual logic for calculating factorial
// Multiple numbers from 1 to n to figure out factorial
         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
